package com.hillel.furnitureMaker.core.application.controller.login;


import com.hillel.furnitureMaker.config.util.JwtUtil;
import com.hillel.furnitureMaker.core.application.dto.LoginRequestDto;
import com.hillel.furnitureMaker.core.application.dto.LoginResponseDto;
import com.hillel.furnitureMaker.core.application.dto.NewUserDto;
import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.application.exceptions.UserWithSuchEmailExistException;
import com.hillel.furnitureMaker.core.application.facade.UserFacade;
import com.hillel.furnitureMaker.core.domain.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(value = "Login and user registration", tags = "Login and registration", protocols = "/transport")
@RestController
public class LoginController {
    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;
    private final UserDetailsService userDetailsService;
    private final UserFacade userFacade;

    public LoginController(AuthenticationManager authenticationManager,
                           JwtUtil jwtUtil,
                           UserService userDetailsService, UserFacade userFacade) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.userDetailsService = userDetailsService;
        this.userFacade = userFacade;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody LoginRequestDto loginRequestDto) throws Exception {
        authenticate(loginRequestDto.getUsername(), loginRequestDto.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequestDto.getUsername());
        final String token = jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok(new LoginResponseDto(token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto saveNewUser(@RequestBody @Valid NewUserDto newUserDto) {
        String email = newUserDto.getEmail();
        if (userFacade.isUserExistByEmail(email)) {
            throw new UserWithSuchEmailExistException(email);
        }

        return userFacade.saveNewUser(newUserDto);
    }
}
