package com.hillel.furnitureMaker.core.database.repository;

import com.hillel.furnitureMaker.core.database.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

    @Override
    <S extends UserEntity> S save(S entity);

    @Override
    void deleteById(Long aLong);

    @Override
    Optional<UserEntity> findById(Long aLong);

    @Override
    Page<UserEntity> findAll(Pageable pageable);

    @Override
    Iterable<UserEntity> findAll(Sort sort);

    boolean existsByEmail(String email);

    Optional<UserEntity> findUserByEmail(String email);


    Iterable<UserEntity>  findAllByFirstNameAndLastName(String firstName, String lastName);
}
