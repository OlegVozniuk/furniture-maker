package com.hillel.furnitureMaker.core.application.dto;

import com.hillel.furnitureMaker.core.database.entity.Role;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessLevelDto {
    Role role;
}
