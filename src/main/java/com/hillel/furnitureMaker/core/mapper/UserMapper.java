package com.hillel.furnitureMaker.core.mapper;


import com.hillel.furnitureMaker.core.application.dto.NewUserDto;
import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.database.entity.UserEntity;
import com.hillel.furnitureMaker.core.domain.model.NewUser;
import com.hillel.furnitureMaker.core.domain.model.User;
import com.hillel.furnitureMaker.core.domain.model.UserSecurity;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Objects;


@Component
public class UserMapper {
    private final PasswordEncoder passwordEncoder;

    private final ModelMapper mapper;

    public UserMapper(PasswordEncoder passwordEncoder, ModelMapper mapper) {
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
    }

    public UserDto toDto(User user) {
        return Objects.isNull(user) ? null : mapper.map(user, UserDto.class);
    }

    public User toModel(UserEntity userEntity) {
        return Objects.isNull(userEntity) ? null : mapper.map(userEntity, User.class);
    }

    public UserSecurity toUserSecurity(UserEntity userEntity) {
        return Objects.isNull(userEntity) ? null : mapper.map(userEntity, UserSecurity.class);
    }

    public NewUser toNewUserFromNewUserDto(NewUserDto newUserDto) {
        return Objects.isNull(newUserDto) ? null : mapper.map(newUserDto, NewUser.class);
    }

    public UserEntity toEntityFromNewUser(NewUser newUser) {
        if (Objects.isNull(newUser)) {
            return null;
        }

        UserEntity userEntity = mapper.map(newUser, UserEntity.class);
        userEntity.setPassword(passwordEncoder.encode(newUser.getPassword()));

        return userEntity;
    }

    public User toUserFromUserDto(UserDto userDto) {
        return Objects.isNull(userDto) ? null : mapper.map(userDto, User.class);
    }
}
