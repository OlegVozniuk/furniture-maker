package com.hillel.furnitureMaker.core.domain.model;

import com.hillel.furnitureMaker.core.database.entity.Address;
import com.hillel.furnitureMaker.core.database.entity.Bucket;
import com.hillel.furnitureMaker.core.database.entity.Gender;
import com.hillel.furnitureMaker.core.database.entity.Role;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class NewUser {
    private int age;

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Address address;

    private Gender gender;
    private Role role;

    private List<Bucket> buckets;
}
