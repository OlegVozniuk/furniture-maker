package com.hillel.furnitureMaker.core.database.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Component;

import javax.persistence.*;


@ApiModel(description = "Order delivery address")
@Data
@ToString
@Entity
@Table(name = "address")
@Component
public class Address {
    @ApiModelProperty(value = "Address id in the database", example = "1", required = true)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @ApiModelProperty(value = "City to deliver order", example = "Kyiv", required = true)
    private String city;

    @ApiModelProperty(value = "Street to deliver order", example = "Khreshchatyk", required = true)
    private String street;

    @ApiModelProperty(value = "Build number to deliver order", example = "1/2", required = true)
    @Column(name = "build_number")
    private Integer buildNumber;

    @ApiModelProperty(value = "User id", example = "1", required = true)
    @OneToOne(mappedBy = "address")
    private UserEntity userEntity;
}
