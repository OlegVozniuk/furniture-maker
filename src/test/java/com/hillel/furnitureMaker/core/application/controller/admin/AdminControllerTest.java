package com.hillel.furnitureMaker.core.application.controller.admin;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.furnitureMaker.FurnitureMakerApplication;
import com.hillel.furnitureMaker.core.application.dto.AccessLevelDto;
import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.database.entity.Role;
import com.hillel.furnitureMaker.core.database.entity.UserEntity;
import com.hillel.furnitureMaker.core.database.repository.UserRepository;
import com.hillel.furnitureMaker.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {FurnitureMakerApplication.class})
@AutoConfigureWireMock(port = 6065, stubs = "classpath:/wiremock/stubs", files = "classpath:/wiremock")
class AdminControllerTest {
    public static final int NOT_EXIST_ID = 0;
    @Autowired
    private AdminController adminController;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(adminController).build();
    }

    @Test
    void getAllUsersFeign() throws Exception {
        //given
        int feignNumberOfUsers = 6;

        //when
        MockHttpServletRequestBuilder builder = get("/admin/feign")
                .contentType(APPLICATION_JSON);

        //then
        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(feignNumberOfUsers)));
    }

    @Test
    void removeUserById() throws Exception {
        //given
        UserEntity userEntity = userRepository.save(new UserEntity());

        //when
        MockHttpServletRequestBuilder delete = delete("/admin/deleteuser/{id}", userEntity.getId());

        //then
        mockMvc.perform(delete)
                .andExpect(status().isAccepted());
    }

    @Test
    void shouldThrowExceptionWhenRemoveUserByNotExistId() {
        //given
        //when
        MockHttpServletRequestBuilder delete = delete("/admin/deleteuser/{id}", NOT_EXIST_ID);

        //then
        Exception exception = assertThrows(Exception.class, () -> mockMvc.perform(delete));

        assertEquals(String.format("Could not found user with id %s", NOT_EXIST_ID),
                exception.getCause().getMessage());
    }

    @Test
    void getAllUsers() throws Exception {
        //given
        List<UserEntity> userEntities = StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());


        //when
        MockHttpServletRequestBuilder content = get("/admin/allusers")
                .contentType(APPLICATION_JSON);

        //then
        mockMvc.perform(content)
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(userEntities.size())))
                .andReturn();
    }


    @Test
    void getAllUsersByFirstNameAndLastName() throws Exception {
        //given
        int usersWithSameFirstAndLastNameCount = 2;

        String firstName = "FirstName";
        String lastName = "LastName";

        UserEntity userEntityA = new UserEntity();
        UserEntity userEntityB = new UserEntity();
        UserEntity userEntityC = new UserEntity();

        userEntityA.setFirstName(firstName);
        userEntityA.setLastName(lastName);
        userEntityB.setFirstName(firstName);
        userEntityB.setLastName(lastName);

        userRepository.save(userEntityA);
        userRepository.save(userEntityC);
        userRepository.save(userEntityB);

        //when
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder =
                get(String.format("/admin/findUserByFirstAndSecondName?firstName=%s&secondName=%s",
                        firstName, lastName))
                        .contentType(APPLICATION_JSON);

        //then
        MvcResult mvcResult = mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(usersWithSameFirstAndLastNameCount)))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();

        List<UserDto> userDtos = objectMapper.readValue(contentAsString, new TypeReference<List<UserDto>>() {
        });

        for (UserDto userDto : userDtos) {
            assertEquals(firstName, userDto.getFirstName());
            assertEquals(firstName, userDto.getFirstName());
        }
    }

    @Test
    void shouldThrowExceptionIfNotFoundUsersByFirstAndLastName() {
        //given
        //when
        String notExistUserFirstName = "noFirstName";
        String notExistUserLastName = "noLastName";
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder =
                get(String.format("/admin/findUserByFirstAndSecondName?firstName=%s&secondName=%s",
                        notExistUserFirstName, notExistUserLastName))
                        .contentType(APPLICATION_JSON);

        //then
        Exception userNotFoundByFirstAndSecondNameException = assertThrows(Exception.class,
                () -> mockMvc.perform(mockHttpServletRequestBuilder));

        assertEquals(String.format("User %s %s not found", notExistUserFirstName, notExistUserLastName),
                userNotFoundByFirstAndSecondNameException.getCause().getMessage());
    }

    @Test
    void shouldSetAccessLevelUSER() throws Exception {
        //given
        UserEntity userEntity = userRepository.save(new UserEntity());
        Long id = userEntity.getId();

        AccessLevelDto accessLevelDto = new AccessLevelDto();
        accessLevelDto.setRole(Role.USER);

        byte[] bytes = TestUtil.convertObjectToJsonBytes(accessLevelDto);

        //when
        MockHttpServletRequestBuilder content = patch("/admin/setAccessLevel/{id}", id)
                .contentType(APPLICATION_JSON)
                .content(bytes);

        //then
        mockMvc.perform(content)
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON));

        Optional<UserEntity> byId = userRepository.findById(id);

        byId.ifPresent(entity -> assertEquals(Role.USER, entity.getRole()));
    }

    @Test
    void shouldSetAccessLevelADMIN() throws Exception {
        //given
        UserEntity userEntity = userRepository.save(new UserEntity());
        Long id = userEntity.getId();

        AccessLevelDto accessLevelDto = new AccessLevelDto();
        accessLevelDto.setRole(Role.ADMIN);

        byte[] bytes = TestUtil.convertObjectToJsonBytes(accessLevelDto);

        //when
        MockHttpServletRequestBuilder content = patch("/admin/setAccessLevel/{id}", id)
                .contentType(APPLICATION_JSON)
                .content(bytes);

        //then
        mockMvc.perform(content)
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON));

        Optional<UserEntity> byId = userRepository.findById(id);

        byId.ifPresent(entity -> assertEquals(Role.ADMIN, entity.getRole()));
    }
}