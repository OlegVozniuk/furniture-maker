package com.hillel.furnitureMaker.core.application.facade;

import com.hillel.furnitureMaker.core.application.dto.AccessLevelDto;
import com.hillel.furnitureMaker.core.application.dto.NewUserDto;
import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.application.exceptions.UserNotFoundByIdException;
import com.hillel.furnitureMaker.core.domain.model.User;
import com.hillel.furnitureMaker.core.domain.service.UserService;
import com.hillel.furnitureMaker.core.mapper.UserMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserFacade {
    private final UserService userService;
    private final UserMapper userMapper;

    public UserFacade(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public List<UserDto> getAllUsers() {
        return userService.getAllUsers().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    public List<UserDto> getAllUsersFeign() {
        return userService.getAllUsersFeign().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    public UserDto getUserById(Long id) {
        return userMapper.toDto(userService.getUserById(id));
    }

    public void removeUserById(Long id) {
        checkUserInDB(id);

        userService.removeUserById(id);
    }

    public boolean isUserExistByEmail(String email) {
        return userService.isUserEmailExist(email);
    }

    public UserDto getUserByEmail(String email) {
        return userMapper.toDto(userService.getUserByEmail(email));
    }

    public List<UserDto> getAllUsersByFirstNameAndLastName(String firstName, String lastName) {
        return userService.getAllUsersByFirstNameAndLastName(firstName, lastName).stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    public UserDto saveNewUser(NewUserDto newUserDto) {
        return userMapper.toDto(userService.saveNewUser(userMapper.toNewUserFromNewUserDto(newUserDto)));
    }

    public UserDto updateUserData(Long id, UserDto userDto) {
        checkUserInDB(id);

        User user = userMapper.toUserFromUserDto(userDto);

        return userMapper.toDto(userService.update(id, user));
    }

    public UserDto updateAccessLevel(Long id, AccessLevelDto accessLevelDto) {
        checkUserInDB(id);

        User user = userService.setAccessLevel(id, accessLevelDto);

        return userMapper.toDto(user);
    }

    private void checkUserInDB(Long id) {
        if (!userService.isUserExistById(id)) {
            throw new UserNotFoundByIdException(id);
        }
    }
}