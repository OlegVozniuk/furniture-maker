package com.hillel.furnitureMaker.core.application.controller.admin;

import com.hillel.furnitureMaker.core.application.dto.AccessLevelDto;
import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.application.exceptions.UserNotFoundByFirstAndSecondNameException;
import com.hillel.furnitureMaker.core.application.facade.UserFacade;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "Admin controller", tags = "Admin level operations", protocols = "/transport")
@RestController
@RequestMapping("/admin")
public class AdminController {
    final private UserFacade userFacade;

    public AdminController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @DeleteMapping("/deleteuser/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void removeUserById(@PathVariable Long id) {
        userFacade.removeUserById(id);
    }

    @GetMapping("/allusers")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsers() {
        return userFacade.getAllUsers();
    }

    @GetMapping("/feign")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsersFeign() {
        return userFacade.getAllUsersFeign();
    }

    @GetMapping("/findUserByFirstAndSecondName")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsersByFirstNameAndLastName(@RequestParam(required = false) String firstName,
                                                           @RequestParam(required =  false) String secondName) {
        List<UserDto> allUsersByFirstNameAndLastName = userFacade
                .getAllUsersByFirstNameAndLastName(firstName, secondName);

        if (allUsersByFirstNameAndLastName.size() == 0) {
            throw new UserNotFoundByFirstAndSecondNameException(firstName, secondName);
        }

        return allUsersByFirstNameAndLastName;
    }

    @PatchMapping(value = "/setAccessLevel/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto setAccessLevel(@PathVariable Long id, @RequestBody AccessLevelDto accessLevelDto) {
        return userFacade.updateAccessLevel(id, accessLevelDto);
    }
}
