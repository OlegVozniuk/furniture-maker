package com.hillel.furnitureMaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class FurnitureMakerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FurnitureMakerApplication.class, args);
    }

}
