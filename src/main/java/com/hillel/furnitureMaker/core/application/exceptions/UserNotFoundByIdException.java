package com.hillel.furnitureMaker.core.application.exceptions;

public class UserNotFoundByIdException extends RuntimeException {
    public UserNotFoundByIdException(Long id) {
        super("Could not found user with id " + id);
    }
}
