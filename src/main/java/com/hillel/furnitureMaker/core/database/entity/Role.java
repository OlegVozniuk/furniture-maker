package com.hillel.furnitureMaker.core.database.entity;

public enum Role {
    ADMIN, USER
}
