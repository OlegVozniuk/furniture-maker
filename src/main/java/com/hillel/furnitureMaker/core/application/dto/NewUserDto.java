package com.hillel.furnitureMaker.core.application.dto;

import com.hillel.furnitureMaker.core.database.entity.Address;
import com.hillel.furnitureMaker.core.database.entity.Gender;
import com.hillel.furnitureMaker.core.database.entity.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@ApiModel(value = "Add User Request", description = "Request Model for creating user")
@Getter
@Setter
public class NewUserDto {
    @ApiModelProperty(value = "User age", example = "26", required = true)
    @Min(18)
    private int age;

    @ApiModelProperty(value = "User first name", example = "Jon", required = true)
    @NotNull
    private String firstName;

    @ApiModelProperty(value = "User last name", example = "Smith", required = true)
    @NotNull
    private String lastName;

    @ApiModelProperty(value = "User email", example = "email@gmail.com", required = true)
    @Email
    private String email;

    @ApiModelProperty(value = "User password", example = "f2fdeDw5gB", required = true)
    @NotNull
    private String password;

    @ApiModelProperty(value = "Order delivery address ")
    private Address address;

    @ApiModelProperty(value = "User gender", example = "MALE", required = true)
    @NotNull
    private Gender gender;

    @ApiModelProperty(value = "User excess level", example = "USER", required = true)
    @NotNull
    private Role role;
}
