package com.hillel.furnitureMaker.core.application.controller.exeptions;

import com.hillel.furnitureMaker.core.application.dto.ApiError;
import com.hillel.furnitureMaker.core.application.exceptions.UserNotFoundByEmailException;
import com.hillel.furnitureMaker.core.application.exceptions.UserNotFoundByFirstAndSecondNameException;
import com.hillel.furnitureMaker.core.application.exceptions.UserNotFoundByIdException;
import com.hillel.furnitureMaker.core.application.exceptions.UserWithSuchEmailExistException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionController {

    @ResponseBody
    @ExceptionHandler(UserNotFoundByIdException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ApiError userNotFoundByIdHandler(UserNotFoundByIdException e) {
        return new ApiError(LocalDateTime.now(), e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(UserNotFoundByEmailException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ApiError userNotFoundByEmailHandler(UserNotFoundByEmailException e) {
        return new ApiError(LocalDateTime.now(), e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(UserNotFoundByFirstAndSecondNameException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ApiError userNotFoundByFirstAndSecondNameHandler(UserNotFoundByFirstAndSecondNameException e) {
        return new ApiError(LocalDateTime.now(), e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(UserWithSuchEmailExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ApiError userWithSuchEmailExistException(UserWithSuchEmailExistException e) {
        return new ApiError(LocalDateTime.now(), e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ApiError userWithSuchEmailExistException(MethodArgumentNotValidException e) {
        return new ApiError(LocalDateTime.now(), e.getMessage());
    }
}
