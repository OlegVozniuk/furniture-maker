package com.hillel.furnitureMaker.util;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestListUtil {
    public static void assertListEquals(List listA, List listB) {

        assertEquals(listB.size(), listA.size());

        for (int i = 0; i < listB.size(); i++) {
            Class<?> aClass = listA.get(i).getClass();
            Class<?> bClass = listB.get(i).getClass();

            Field[] declaredFieldsA = aClass.getDeclaredFields();
            Field[] declaredFieldsB = bClass.getDeclaredFields();

            assertEquals(declaredFieldsA.length, declaredFieldsB.length);

            for (int j = 0; j < declaredFieldsA.length; j++) {
                try {
                    Field fieldA = declaredFieldsA[j];
                    Field fieldB = declaredFieldsB[j];

                    fieldA.setAccessible(true);
                    fieldB.setAccessible(true);

                    assertEquals(fieldA.get(listA.get(i)), fieldB.get(listB.get(i)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
