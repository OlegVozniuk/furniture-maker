package com.hillel.furnitureMaker.core.domain.service;

import com.hillel.furnitureMaker.core.database.entity.UserEntity;
import com.hillel.furnitureMaker.core.database.repository.UserRepository;
import com.hillel.furnitureMaker.core.domain.model.User;
import com.hillel.furnitureMaker.core.domain.model.UserSecurity;
import com.hillel.furnitureMaker.core.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.hillel.furnitureMaker.util.TestListUtil.assertListEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    public static final String NOT_EXISTING_TEST_EMAIL = "notExistingTestEmail@gmail.com";
    public static final String EMAIL = "email@gmail.com";

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @Test
    void shouldLoadUserByUsername() {
        //given
        UserEntity userEntity = new UserEntity();

        UserSecurity userSecurity = new UserSecurity();
        userSecurity.setEmail(EMAIL);

        //when
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(userEntity));
        when(userMapper.toUserSecurity(any())).thenReturn(userSecurity);

        UserDetails userDetails = userService.loadUserByUsername(EMAIL);

        //then
        assertEquals(EMAIL, userDetails.getUsername());

        verify(userRepository, times(1)).findUserByEmail(anyString());
        verifyNoMoreInteractions(userRepository);

        verify(userMapper, times(1)).toUserSecurity(any());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    void shouldThrowExceptionWhenUserNotFound() {
        //given
        UserSecurity userSecurity = new UserSecurity();
        userSecurity.setEmail(EMAIL);

        //when
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.empty());

        //then
        UsernameNotFoundException usernameNotFoundException = assertThrows(UsernameNotFoundException.class,
                () -> userService.loadUserByUsername(NOT_EXISTING_TEST_EMAIL));

        assertEquals("Can not found user with such email: " + NOT_EXISTING_TEST_EMAIL
                , usernameNotFoundException.getMessage());
    }

    @Test
    void shouldFindAndReturnUserById() {
        //given
        Long userId = 13L;
        UserEntity userEntity = new UserEntity();
        User user = new User();

        user.setId(userId);

        //when
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(userEntity));
        when(userMapper.toModel(any())).thenReturn(user);

        User userById = userService.getUserById(userId);

        //then
        assertEquals(userId, userById.getId());

        verify(userRepository, times(1)).findById(anyLong());
        verifyNoMoreInteractions(userRepository);

        verify(userMapper, times(1)).toModel(any());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    void shouldReturnTrueIfUserWithSuchIdExist() {
        //given
        Long id = 15L;

        //when
        when(userRepository.existsById(anyLong())).thenReturn(true);
        boolean userExistById = userService.isUserExistById(id);

        //then
        assertTrue(userExistById);
    }

    @Test
    void shouldReturnFalseIfUserWithSuchIdNotExist() {
        //given
        Long id = 15L;

        //when
        when(userRepository.existsById(anyLong())).thenReturn(false);
        boolean userExistById = userService.isUserExistById(id);

        //then
        assertFalse(userExistById);
    }

    @Test
    void shouldReturnTrueIfUserWithSuchEmailExist() {
        //given
        //when
        when(userRepository.existsByEmail(anyString())).thenReturn(true);
        boolean userEmailExist = userService.isUserEmailExist(EMAIL);

        //then
        assertTrue(userEmailExist);
    }

    @Test
    void shouldReturnFalseIfUserWithSuchEmailNotExist() {
        //given
        //when
        when(userRepository.existsByEmail(anyString())).thenReturn(false);
        boolean userEmailExist = userService.isUserEmailExist(EMAIL);

        //then
        assertFalse(userEmailExist);
    }

    @Test
    void shouldGetAndReturnUserByEmailIfUserExist() {
        //given
        UserEntity userEntity = new UserEntity();
        User user = new User();
        user.setEmail(EMAIL);

        //when
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.of(userEntity));
        when(userMapper.toModel(any())).thenReturn(user);

        User userByEmail = userService.getUserByEmail(EMAIL);

        //then
        assertEquals(EMAIL, userByEmail.getEmail());

        verify(userRepository, times(1)).findUserByEmail(anyString());
        verifyNoMoreInteractions(userRepository);

        verify(userMapper, times(1)).toModel(any());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    void shouldReturnNullWhenUserNotExistByEmail() {
        //given
        //when
        when(userRepository.findUserByEmail(anyString())).thenReturn(Optional.empty());
        when(userMapper.toModel(any())).thenReturn(null);

        User userByEmail = userService.getUserByEmail(EMAIL);

        //then
        assertNull(userByEmail);

        verify(userRepository, times(1)).findUserByEmail(anyString());
        verifyNoMoreInteractions(userRepository);

        verify(userMapper, times(1)).toModel(any());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    void shouldFindAndReturnAllUsersWithSuchFirstAndLastName() {
        //given
        String firstName = "Jon";
        String lastName = "Smith";

        ArrayList<UserEntity> userEntities = new ArrayList<>();
        ArrayList<User> users = new ArrayList<>();

        User userA = new User();
        User userB = new User();

        userA.setFirstName(firstName);
        userA.setLastName(lastName);
        userB.setFirstName(firstName);
        userB.setLastName(lastName);

        userEntities.add(new UserEntity());
        userEntities.add(new UserEntity());

        users.add(userA);
        users.add(userB);

        //when
        when(userRepository.findAllByFirstNameAndLastName(firstName, lastName)).thenReturn(userEntities);

        for (int i = 0; i < userEntities.size(); i++) {
            when(userMapper.toModel(any())).thenReturn(users.get(i));
        }

        List<User> allUsersByFirstNameAndLastName = userService.getAllUsersByFirstNameAndLastName(firstName, lastName);

        //then
        assertListEquals(users, allUsersByFirstNameAndLastName);
    }
}