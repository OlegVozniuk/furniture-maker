package com.hillel.furnitureMaker.core.application.facade;

import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.domain.model.User;
import com.hillel.furnitureMaker.core.domain.service.UserService;
import com.hillel.furnitureMaker.core.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.hillel.furnitureMaker.util.TestListUtil.assertListEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserFacadeTest {
    public static final long ID = 1L;
    public static final String TEST_EMAIL = "testEmail@email.com";
    @InjectMocks
    UserFacade userFacade;

    @Mock
    UserService userService;

    @Mock
    UserMapper userMapper;

    @Test
    void shouldReturnUserById() {
        //given
        UserDto userDto = new UserDto();
        userDto.setId(ID);

        User user = new User();

        //when
        when(userService.getUserById(anyLong())).thenReturn(user);
        when(userMapper.toDto(any())).thenReturn(userDto);

        UserDto userById = userFacade.getUserById(ID);

        //then
        assertEquals(ID, userById.getId());

        verify(userService, times(1)).getUserById(anyLong());
        verifyNoMoreInteractions(userService);

        verify(userMapper, times(1)).toDto(any());
        verifyNoMoreInteractions(userMapper);
    }

    @Test
    void shouldReturnTrueIfUserWithSuchEmailExist() {
        //given
        //when
        when(userService.isUserEmailExist(anyString())).thenReturn(true);
        boolean userExistByEmail = userFacade.isUserExistByEmail(TEST_EMAIL);

        //then
        assertTrue(userExistByEmail);
        verify(userService, times(1)).isUserEmailExist(anyString());
        verifyNoMoreInteractions(userService);
    }

    @Test
    void shouldReturnFalseIfUserWithSuchEmailExist() {
        //given
        //when
        when(userService.isUserEmailExist(anyString())).thenReturn(false);
        boolean userExistByEmail = userFacade.isUserExistByEmail(TEST_EMAIL);

        //then
        assertFalse(userExistByEmail);
        verify(userService, times(1)).isUserEmailExist(anyString());
        verifyNoMoreInteractions(userService);
    }

    @Test
    void shouldReturnUserByEmail() {
        //given
        UserDto userDto = new UserDto();
        userDto.setEmail(TEST_EMAIL);
        User user = new User();

        //when
        when(userService.getUserByEmail(anyString())).thenReturn(user);
        when(userMapper.toDto(any())).thenReturn(userDto);
        UserDto userByEmail = userFacade.getUserByEmail(TEST_EMAIL);

        //then
        assertEquals(TEST_EMAIL, userByEmail.getEmail());

        verify(userService, times(1)).getUserByEmail(anyString());
        verifyNoMoreInteractions(userService);
    }

    @Test
    void getAllUsersByFirstNameAndLastName() {
        //given
        String firstName = "Jon";
        String lastName = "Smith";

        ArrayList<User> users = new ArrayList<>();
        ArrayList<UserDto> userDtos = new ArrayList<>();

        UserDto userDtoA = new UserDto();
        UserDto userDtoB = new UserDto();

        userDtoA.setFirstName(firstName);
        userDtoA.setLastName(lastName);
        userDtoB.setFirstName(firstName);
        userDtoB.setLastName(lastName);

        userDtos.add(userDtoA);
        userDtos.add(userDtoB);

        users.add(new User());
        users.add(new User());

        //when
        when(userService.getAllUsersByFirstNameAndLastName(anyString(), anyString())).thenReturn(users);

        for (int i = 0; i < users.size(); i++) {
            when(userMapper.toDto(any())).thenReturn(userDtos.get(i));
        }

        List<UserDto> allUsersByFirstNameAndLastName = userFacade.getAllUsersByFirstNameAndLastName(firstName, lastName);

        //then
        assertListEquals(userDtos, allUsersByFirstNameAndLastName);
    }
}