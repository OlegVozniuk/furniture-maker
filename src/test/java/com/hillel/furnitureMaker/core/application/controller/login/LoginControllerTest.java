package com.hillel.furnitureMaker.core.application.controller.login;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.furnitureMaker.FurnitureMakerApplication;
import com.hillel.furnitureMaker.core.application.dto.LoginRequestDto;
import com.hillel.furnitureMaker.core.application.dto.NewUserDto;
import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.database.entity.Gender;
import com.hillel.furnitureMaker.core.database.entity.Role;
import com.hillel.furnitureMaker.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {FurnitureMakerApplication.class})
class LoginControllerTest {
    private static final String TEST_EMAIL = "JonSmith@gmail.com";
    private static final String NOT_EXIST_EMAIL = "notExistEmail@email.com";
    private static final String PASSWORD = "1234";
    private static final int AGE = 23;
    private static final String FIRST_NAME = "Jon";
    private static final String LAST_NAME = "Smith";
    private static final Gender GENDER = Gender.MALE;
    private static final Role ROLE = Role.USER;
    public static final String WRONG_PASSWORD = "1w34";


    @Autowired
    private LoginController loginController;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;
    private NewUserDto newUserDto;

    @BeforeEach
    void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(loginController).build();

        newUserDto = new NewUserDto();
        newUserDto.setAge(AGE);
        newUserDto.setFirstName(FIRST_NAME);
        newUserDto.setLastName(LAST_NAME);
        newUserDto.setEmail(TEST_EMAIL);
        newUserDto.setPassword(PASSWORD);
        newUserDto.setGender(Gender.MALE);
        newUserDto.setRole(ROLE);
        newUserDto.setGender(GENDER);
    }

    @Order(2)
    @Test
    void login() throws Exception {
        //given
        LoginRequestDto loginRequestDto = new LoginRequestDto();
        loginRequestDto.setUserName(TEST_EMAIL);
        loginRequestDto.setPassword(PASSWORD);


        byte[] bytes = TestUtil.convertObjectToJsonBytes(loginRequestDto);


        //when
        MockHttpServletRequestBuilder postRequest = post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bytes);

        //then
        mockMvc.perform(postRequest)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Order(1)
    @Test
    void saveNewUser() throws Exception {
        //given
        byte[] bytes = TestUtil.convertObjectToJsonBytes(newUserDto);

        //when
        MockHttpServletRequestBuilder postRequest = post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bytes);

        //Then
        MvcResult mvcResult = mockMvc.perform(postRequest)
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
        UserDto userDto = objectMapper.readValue(contentAsString, UserDto.class);

        assertEquals(TEST_EMAIL, userDto.getEmail());
    }

    @Order(3)
    @Test
    void shouldThrowExceptionWhenTrySaveNewUserWithExistEmail() throws Exception {
        //given
        byte[] bytes = TestUtil.convertObjectToJsonBytes(newUserDto);

        //when
        MockHttpServletRequestBuilder postRequest = post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bytes);

        //Then
        Exception exception = assertThrows(Exception.class, () -> mockMvc.perform(postRequest));

        assertEquals(String.format("User with such email exist: %s", TEST_EMAIL),
                exception.getCause().getMessage());
    }

    @Order(4)
    @Test
    void shouldThrowExceptionWhenLoginWithNotExistingEmail() throws Exception {
        //given
        LoginRequestDto loginRequestDto = new LoginRequestDto();
        loginRequestDto.setUserName(NOT_EXIST_EMAIL);
        loginRequestDto.setPassword(PASSWORD);


        byte[] bytes = TestUtil.convertObjectToJsonBytes(loginRequestDto);


        //when
        MockHttpServletRequestBuilder postRequest = post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bytes);

        //then
        Exception exception = assertThrows(Exception.class, () -> mockMvc.perform(postRequest));

        assertEquals("INVALID_CREDENTIALS", exception.getCause().getMessage());
    }

    @Order(5)
    @Test
    void shouldThrowExceptionWhenLoginWithNotWrongPassword() throws Exception {
        //given
        LoginRequestDto loginRequestDto = new LoginRequestDto();
        loginRequestDto.setUserName(TEST_EMAIL);
        loginRequestDto.setPassword(WRONG_PASSWORD);


        byte[] bytes = TestUtil.convertObjectToJsonBytes(loginRequestDto);


        //when
        MockHttpServletRequestBuilder postRequest = post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bytes);

        //then
        Exception exception = assertThrows(Exception.class, () -> mockMvc.perform(postRequest));

        assertEquals("INVALID_CREDENTIALS", exception.getCause().getMessage());
    }
}