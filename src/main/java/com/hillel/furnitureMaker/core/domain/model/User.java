package com.hillel.furnitureMaker.core.domain.model;

import com.hillel.furnitureMaker.core.database.entity.Gender;
import com.hillel.furnitureMaker.core.database.entity.Role;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    private Long id;
    private Integer age;
    private Gender gender;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
}
