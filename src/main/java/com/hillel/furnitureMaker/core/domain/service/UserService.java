package com.hillel.furnitureMaker.core.domain.service;

import com.hillel.furnitureMaker.client.someapi.SomeClient;
import com.hillel.furnitureMaker.core.application.dto.AccessLevelDto;
import com.hillel.furnitureMaker.core.application.exceptions.UserNotFoundByIdException;
import com.hillel.furnitureMaker.core.database.entity.Role;
import com.hillel.furnitureMaker.core.database.entity.UserEntity;
import com.hillel.furnitureMaker.core.database.repository.UserRepository;
import com.hillel.furnitureMaker.core.domain.model.NewUser;
import com.hillel.furnitureMaker.core.domain.model.User;
import com.hillel.furnitureMaker.core.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService implements UserDetailsService {
    private final SomeClient someClient;

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    @Autowired
    public UserService(UserRepository userRepository, UserMapper userMapper, SomeClient someClient) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.someClient = someClient;
    }

    public List<User> getAllUsersFeign() {
        return someClient.getAllUsersFeign();
    }

    public List<User> getAllUsers() {
        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .map(userMapper::toModel)
                .collect(Collectors.toList());
    }

    public void removeUserById(Long id) {
        userRepository.deleteById(id);
    }

    public User getUserById(Long id) {
        return userMapper.toModel(userRepository.findById(id).orElse(null));
    }

    public boolean isUserExistById(Long id) {
        return userRepository.existsById(id);
    }

    public boolean isUserEmailExist(String email) {
        return userRepository.existsByEmail(email);
    }

    public User getUserByEmail(String email) {
        return userMapper.toModel(userRepository.findUserByEmail(email).orElse(null));
    }

    public List<User> getAllUsersByFirstNameAndLastName(String firstName, String lastName) {
        return StreamSupport
                .stream(userRepository.findAllByFirstNameAndLastName(firstName, lastName).spliterator(), false)
                .map(userMapper::toModel)
                .collect(Collectors.toList());
    }

    public User saveNewUser(NewUser newUser) {
        newUser.setRole(Role.USER);

        return userMapper.toModel(userRepository.save(userMapper.toEntityFromNewUser(newUser)));
    }

    public User update(Long id, User user) {
        UserEntity userEntity = userRepository.findById(id).orElse(null);

        if (userEntity == null) {
            throw new UserNotFoundByIdException(id);
        }

        Class<? extends User> newUserData = user.getClass();

        Arrays.stream(newUserData.getDeclaredFields())
                .forEach(field -> {
                    Field entityField = ReflectionUtils.findField(UserEntity.class, field.getName());

                    if (entityField != null) {
                        field.setAccessible(true);
                        entityField.setAccessible(true);

                        try {
                            ReflectionUtils.setField(entityField, userEntity, field.get(user));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                });

        UserEntity save = userRepository.save(userEntity);

        return userMapper.toModel(save);
    }

    public User setAccessLevel(Long id, AccessLevelDto accessLevelDto) {
        UserEntity userEntity = userRepository.findById(id).orElse(null);

        userEntity.setRole(accessLevelDto.getRole());

        userRepository.save(userEntity);

        return userMapper.toModel(userEntity);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findUserByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Can not found user with such email: " + email));

        return userMapper.toUserSecurity(userEntity);
    }
}
