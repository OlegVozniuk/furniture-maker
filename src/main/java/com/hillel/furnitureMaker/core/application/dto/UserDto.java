package com.hillel.furnitureMaker.core.application.dto;

import com.hillel.furnitureMaker.core.database.entity.Address;
import com.hillel.furnitureMaker.core.database.entity.Bucket;
import com.hillel.furnitureMaker.core.database.entity.Gender;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@ApiModel(value = "User data transfer object", description = "Model for get request")
@Getter
@Setter
public class UserDto {
    @ApiModelProperty(value = "User id in the database", example = "1", required = true)
    private long id;

    @ApiModelProperty(value = "User age", example = "26", required = true)
    private int age;

    @ApiModelProperty(value = "User first name", example = "Jon", required = true)
    private String firstName;

    @ApiModelProperty(value = "User last name", example = "Smith", required = true)
    private String lastName;

    @ApiModelProperty(value = "User email", example = "email@gmail.com", required = true)
    private String email;

    @ApiModelProperty(value = "Order delivery address", required = true)
    private Address address;

    @ApiModelProperty(value = "User gender", example = "MALE", required = true)
    private Gender gender;

    @ApiModelProperty(value = "User orders list", required = true)
    private List<Bucket> buckets;
}
