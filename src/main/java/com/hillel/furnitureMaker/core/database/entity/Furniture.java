package com.hillel.furnitureMaker.core.database.entity;

public enum Furniture {
    CHAIR, SOFA, TABLE, WARDROBE
}
