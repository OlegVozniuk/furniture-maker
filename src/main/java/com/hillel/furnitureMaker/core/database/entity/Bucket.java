package com.hillel.furnitureMaker.core.database.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@ApiModel(description = "User orders container")
@Data
@ToString
@Entity
@Table(name = "bucket")
public class Bucket {
    @ApiModelProperty(value = "Bucket id in the database", example = "1", required = true)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @ApiModelProperty(value = "Order", example = "CHAIR", required = true)
    @Enumerated(EnumType.STRING)
    @Column(name = "ordered_furniture")
    private Furniture orderedFurniture;

    @ApiModelProperty(value = "User id", example = "1", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserEntity userEntity;
}
