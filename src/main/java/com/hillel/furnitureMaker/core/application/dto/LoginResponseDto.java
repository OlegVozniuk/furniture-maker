package com.hillel.furnitureMaker.core.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponseDto {
    String token;

    public LoginResponseDto(String token) {
        this.token = token;
    }
}
