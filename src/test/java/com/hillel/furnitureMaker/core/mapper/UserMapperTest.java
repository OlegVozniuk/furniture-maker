package com.hillel.furnitureMaker.core.mapper;

import com.hillel.furnitureMaker.core.application.dto.NewUserDto;
import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.database.entity.UserEntity;
import com.hillel.furnitureMaker.core.domain.model.NewUser;
import com.hillel.furnitureMaker.core.domain.model.User;
import com.hillel.furnitureMaker.core.domain.model.UserSecurity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserMapperTest {
    public static final String ENCODED_PASSWORD = "adfw442$4sd^f_s2";
    @InjectMocks
    private UserMapper userMapper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private ModelMapper modelMapper;

    @Test
    void toDtoMethodShouldReturnUserDtoObjectWhenParameterNotNull() {
        //given
        UserDto userDto = new UserDto();
        User user = new User();

        //when
        when(modelMapper.map(any(), any())).thenReturn(userDto);
        UserDto userDtoResult = userMapper.toDto(user);

        //then
        assertEquals(userDto, userDtoResult);

        verifyModelMapperMapMethodInteractions(1);
    }

    @Test
    void toDtoMethodShouldReturnNullWhenParameterNull() {
        //given
        //when
        UserDto userDto = userMapper.toDto(null);

        //then
        assertNull(userDto);
    }

    @Test
    void toModelMethodShouldReturnUserObjectWhenParameterNotNull() {
        //given
        UserEntity userEntity = new UserEntity();
        User user = new User();

        //when
        when(modelMapper.map(any(), any())).thenReturn(user);
        User userResult = userMapper.toModel(userEntity);

        //then
        assertEquals(user, userResult);

        verifyModelMapperMapMethodInteractions(1);
    }

    @Test
    void toModelMethodShouldReturnNullWhenParameterNull() {
        //given
        //when
        User user = userMapper.toModel(null);

        //then
        assertNull(user);
    }

    @Test
    void toUserSecurityMethodShouldReturnUserSecurityObjectWhenParameterNotNull() {
        //given
        UserEntity userEntity = new UserEntity();
        UserSecurity userSecurity = new UserSecurity();

        //when
        when(modelMapper.map(any(), any())).thenReturn(userSecurity);
        UserSecurity userSecurityResult = userMapper.toUserSecurity(userEntity);

        //then
        assertEquals(userSecurity, userSecurityResult);

        verifyModelMapperMapMethodInteractions(1);
    }

    @Test
    void toUserSecurityMethodShouldNullWhenParameterNull() {
        //given
        //when
        UserSecurity userSecurity = userMapper.toUserSecurity(null);

        //then
        assertNull(userSecurity);
    }

    @Test
    void toNewUserFromNewUserDtoShouldReturnNewUserObjectWhenParameterNotNull() {
        //given
        NewUserDto newUserDto = new NewUserDto();
        NewUser newUser = new NewUser();

        //when
        when(modelMapper.map(any(), any())).thenReturn(newUser);
        NewUser newUserResult = userMapper.toNewUserFromNewUserDto(newUserDto);

        //then
        assertEquals(newUser, newUserResult);

        verifyModelMapperMapMethodInteractions(1);
    }

    @Test
    void toNewUserFromNewUserDtoShouldReturnNullWhenParameterNull() {
        //given
        //when
        NewUser newUser = userMapper.toNewUserFromNewUserDto(null);

        //then
        assertNull(newUser);
    }

    @Test
    void toEntityFromNewUserShouldReturnUserEntityObjectWhenParameterNotNull() {
        //given
        NewUser newUser = new NewUser();
        UserEntity userEntity = new UserEntity();

        newUser.setPassword("1234");

        //when
        when(modelMapper.map(any(), any())).thenReturn(userEntity);
        when(passwordEncoder.encode(anyString())).thenReturn(ENCODED_PASSWORD);

        UserEntity userEntityResult = userMapper.toEntityFromNewUser(newUser);

        //then
        assertEquals(ENCODED_PASSWORD, userEntityResult.getPassword());
        assertEquals(userEntity, userEntityResult);

        verify(passwordEncoder, times(1)).encode(anyString());
        verifyNoMoreInteractions(passwordEncoder);

        verifyModelMapperMapMethodInteractions(1);
    }

    @Test
    void toEntityFromNewUserShouldReturnNullWhenParameterNull() {
        //given
        //when
        UserEntity userEntity = userMapper.toEntityFromNewUser(null);

        //then
        assertNull(userEntity);

    }

    @Test
    void toUserFromUserDtoShouldReturnUserObjectWhenParameterNotNull() {
        //given
        UserDto userDto = new UserDto();
        User user = new User();

        //when
        when(modelMapper.map(any(), any())).thenReturn(user);

        User userResult = userMapper.toUserFromUserDto(userDto);

        //then
        assertEquals(user, userResult);

        verifyModelMapperMapMethodInteractions(1);
    }

    @Test
    void toUserFromUserDtoShouldReturnNullWhenParameterNull() {
        //given
        //when
        User user = userMapper.toUserFromUserDto(null);

        //then
        assertNull(user);
    }

    private void verifyModelMapperMapMethodInteractions(int wantedNumberOfInvocations) {
        verify(modelMapper, times(wantedNumberOfInvocations)).map(any(), any());
        verifyNoMoreInteractions(modelMapper);
    }
}