package com.hillel.furnitureMaker.core.application.exceptions;

public class UserNotFoundByFirstAndSecondNameException extends RuntimeException {
    public UserNotFoundByFirstAndSecondNameException(String firstName, String secondName) {
        super("User " + firstName + " " + secondName + " not found");
    }
}
