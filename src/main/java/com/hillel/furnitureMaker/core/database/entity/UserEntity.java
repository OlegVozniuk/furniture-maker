package com.hillel.furnitureMaker.core.database.entity;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@ToString
@Entity
@Table(name = "furniture_user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "age")
    private int age;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "password")
    private String password;

    @OneToOne(cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.PERSIST,
            fetch = FetchType.LAZY)
    List<Bucket> buckets;

    @Enumerated(EnumType.STRING)
    Role role;
}
