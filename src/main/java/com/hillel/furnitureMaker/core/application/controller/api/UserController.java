package com.hillel.furnitureMaker.core.application.controller.api;

import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.application.exceptions.UserNotFoundByEmailException;
import com.hillel.furnitureMaker.core.application.exceptions.UserNotFoundByIdException;
import com.hillel.furnitureMaker.core.application.facade.UserFacade;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Api(value = "User operations in the System", tags = "Operations with user and user data", protocols = "/transport")
@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserFacade userFacade;

    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getById(@PathVariable Long id) {
        UserDto userById = userFacade.getUserById(id);

        if (userById == null) {
            throw new UserNotFoundByIdException(id);
        }

        return userById;
    }

    @GetMapping("/checkEmail")
    @ResponseStatus(HttpStatus.OK)
    public Boolean existByEmail(@RequestParam(required = false) String email) {
        return userFacade.isUserExistByEmail(email);
    }

    @GetMapping("/findUserByEmail")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getByEmail(@RequestParam(required = false) String email) {
        UserDto userByEmail = userFacade.getUserByEmail(email);

        if (userByEmail == null) {
            throw new UserNotFoundByEmailException(email);
        }

        return userByEmail;
    }

    @PatchMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto update(@PathVariable Long id, @RequestBody UserDto userDto) {
        return userFacade.updateUserData(id, userDto);
    }
}
