package com.hillel.furnitureMaker.core.application.exceptions;

public class UserNotFoundByEmailException extends RuntimeException {
    public UserNotFoundByEmailException(String email) {
            super("Could not found user with email " + email);
    }
}
