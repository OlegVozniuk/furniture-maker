package com.hillel.furnitureMaker.core.application.controller.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.furnitureMaker.FurnitureMakerApplication;
import com.hillel.furnitureMaker.core.application.dto.UserDto;
import com.hillel.furnitureMaker.core.database.entity.UserEntity;
import com.hillel.furnitureMaker.core.database.repository.UserRepository;
import com.hillel.furnitureMaker.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {FurnitureMakerApplication.class})
class UserControllerTest {
    public static final String TEST_EMAIL = "testEmail@email.com";
    public static final String NOT_EXIST_EMAIL = "notExistEmail@email.com";
    public static final Long NOT_EXIST_ID = 0L;
    @Autowired
    private UserController userController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    void getById() throws Exception {
        //given
        UserEntity userEntity = userRepository.save(new UserEntity());

        //when
        MockHttpServletRequestBuilder getRequest =
                get("/api/users/{id}", userEntity.getId())
                        .contentType(MediaType.APPLICATION_JSON);

        //then
        MvcResult mvcResult = mockMvc.perform(getRequest)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();

        UserDto userDto = objectMapper.readValue(contentAsString, UserDto.class);

        assertEquals(userEntity.getId(), userDto.getId());
    }

    @Test
    public void shouldThrowExceptionWhenGetByIdWithNotExistID() {
        //given
        int notExistId = 0;

        //when
        MockHttpServletRequestBuilder getRequest = get("/api/users/{id}", notExistId);
        //then
        Exception exception = assertThrows(Exception.class,
                () -> mockMvc.perform(getRequest));

        assertEquals(String.format("Could not found user with id %s", notExistId),
                exception.getCause().getMessage());

    }

    @Test
    void existByEmailShouldReturnTrueIfUserExist() throws Exception {
        //given
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(TEST_EMAIL);

        userRepository.save(userEntity);

        //when
        MockHttpServletRequestBuilder getRequest = get(String.format("/api/users/checkEmail?email=%s", TEST_EMAIL))
                .contentType(MediaType.APPLICATION_JSON);

        //then
        MvcResult mvcResult = mockMvc.perform(getRequest)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
        Boolean aBoolean = objectMapper.readValue(contentAsString, Boolean.class);

        assertTrue(aBoolean);
    }

    @Test
    void existByEmailShouldReturnFalseIfUserExist() throws Exception {
        //given
        //when
        MockHttpServletRequestBuilder getRequest = get(String.format("/api/users/checkEmail?email=%s", NOT_EXIST_EMAIL))
                .contentType(MediaType.APPLICATION_JSON);

        //then
        MvcResult mvcResult = mockMvc.perform(getRequest)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();
        Boolean aBoolean = objectMapper.readValue(contentAsString, Boolean.class);

        assertFalse(aBoolean);
    }

    @Test
    void getByEmail() throws Exception {
        //given
        UserEntity userEntity = new UserEntity();
        String email = "someEmail@email.com";
        userEntity.setEmail(email);

        userRepository.save(userEntity);

        //when
        MockHttpServletRequestBuilder getRequest = get(String.format("/api/users/findUserByEmail?email=%s", email))
                .contentType(MediaType.APPLICATION_JSON);

        //then
        MvcResult mvcResult = mockMvc.perform(getRequest)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();

        UserDto userDto = objectMapper.readValue(contentAsString, UserDto.class);

        assertEquals(email, userDto.getEmail());
    }

    @Test
    void whenGetByEmailWithNoExistEmailShouldThrowException() {
        //given
        userRepository.save(new UserEntity());

        //when
        MockHttpServletRequestBuilder getRequest =
                get(String.format("/api/users/findUserByEmail?email=%s", NOT_EXIST_EMAIL))
                .contentType(MediaType.APPLICATION_JSON);

        //then
        Exception exception = assertThrows(Exception.class, () -> mockMvc.perform(getRequest));

        assertEquals(String.format("Could not found user with email %s", NOT_EXIST_EMAIL)
                , exception.getCause().getMessage());
    }

    @Test
    void update() throws Exception {
        //given
        UserEntity userEntity = userRepository.save(new UserEntity());
        UserDto userDto = new UserDto();
        String email = "newEmail@email.com";
        userDto.setEmail(email);


        byte[] bytes = TestUtil.convertObjectToJsonBytes(userDto);

        //when
        MockHttpServletRequestBuilder patchRequest = patch("/api/users//{id}", userEntity.getId())
                .contentType(APPLICATION_JSON)
                .content(bytes);

        //then
        MvcResult mvcResult = mockMvc.perform(patchRequest)
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();

        UserDto userDtoUpdate = objectMapper.readValue(contentAsString, UserDto.class);

        assertEquals(userDto.getEmail(), userDtoUpdate.getEmail());
    }

    @Test
    void whenUpdateUserWithNotExistId() throws Exception {
        //given
        UserDto userDto = new UserDto();
        String email = "newEmail@email.com";
        userDto.setEmail(email);


        byte[] bytes = TestUtil.convertObjectToJsonBytes(userDto);

        //when
        MockHttpServletRequestBuilder patchRequest = patch("/api/users//{id}", NOT_EXIST_ID)
                .contentType(APPLICATION_JSON)
                .content(bytes);

        //then
        Exception exception = assertThrows(Exception.class, () -> mockMvc.perform(patchRequest));

        assertEquals(String.format("Could not found user with id %s", NOT_EXIST_ID),
                exception.getCause().getMessage());

    }
}